# [rpc]
# Accept command line and JSON-RPC commands.
server=1
# Accept public REST requests.
rest=1
# Bind to given address to listen for JSON-RPC connections.
rpcbind=[::1]
# Throw a non-fatal error at runtime if the documentation for an RPC is incorrect.
rpcdoccheck=1
# Set the number of threads to service RPC calls
rpcthreads=10
# Number of seconds after which an uncompleted RPC call will time out
rpcservertimeout=120
# Allow JSON-RPC connections from specified source.
rpcallowip=@@ALLOWIP@@
# Credentials
rpcauth=@@RPCAUTH@@

# [core]
datadir=@@DATADIR@@
dbcache=16384
# Maintain coinstats index used by the gettxoutsetinfo RPC.
coinstatsindex=1
# Ensure that we do not daemonize unless explicitly requested.
daemon=@@DC@@
daemonwait=@@DW@@
# Maintain a full transaction index, used by the getrawtransaction rpc call.
txindex=1
# Needed for full validation
assumevalid=0

# [debug]
# How many recent blocks to check at startup.
checkblocks=10
# How thorough the block verification of -checkblocks is (0-4).
checklevel=4
# Run addrman consistency checks every <n> operations.
checkaddrman=100
# Run mempool consistency checks every <n> transactions.
checkmempool=100
# Log IP Addresses in debug output.
logips=1
# Prepend debug output with name of the originating source location (source file, line number and function name).
logsourcelocations=1
# Prepend debug output with name of the originating thread (only available on platforms supporting thread_local).
logthreadnames=1
# Log timestamps with microsecond precision.
logtimemicros=1

# [relay]
# Accept transaction replace-by-fee without requiring replaceability signaling.
mempoolfullrbf=1

# [network]
# Use UPnP to map the listening port.
upnp=0
natpmp=0
onlynet=ipv6

# Allows LND & Bitcoin Core to communicate via ZeroMQ
zmqpubrawblock=tcp://[::]:28332
zmqpubrawtx=tcp://[::]:28333

## Tor Settings
proxy=[::]:9050
bind=[::]
dnsseed=1
dns=1

## Tor v3 Seeds
addnode=kpgvmscirrdqpekbqjsvw5teanhatztpp2gl6eee4zkowvwfxwenqaid.onion
addnode=bnx4povtqynvwnui5oqm5xcxqvat3j7yzgn6mqciuyszqawhuayvyaid.onion:8333
addnode=wyg7twmf7t3pfvfpdcfd64wvjj2pkccuui7ew34ovnqung5f623b4yyd.onion
addnode=glm52zywiqrcxuwswvgjsxr5pfeggil7uci4z5tbpvb4rjyu5hwjhtid.onion
addnode=xspiicyddsdmzxsffzw6z4f22wi2iwyyufkjcaj2qr7cznxymtft5nid.onion
addnode=nqb5q6d4nhp54ziahhm2oxopqwyyulg7nrqwrcmfvwhj7y7xasaeg7ad.onion
addnode=vp4qo7u74cpckygkfoeu4vle2yqmxh7zuxqvtkazp4nbvjnxl2s3e6id.onion
addnode=ew7x2hv76f7w7irfyektnbhd42eut27ttbfgggu7lbpxsyrlfvsujuqd.onion
addnode=qxkgr5u4rmwme5jticjuuqrecw5vfc5thcqdxvbffwbqgqkdin7s7vid.onion

## Tor v2 Seeds
addnode=bitcoinostk4e4re.onion
addnode=bk7yp6epnmcllq72.onion
addnode=qg6embx5o6p4h4it.onion:8333
addnode=trjbor5uc63ghwug.onion:8333
addnode=3eznsanhteo4m2k2.onion:8333
addnode=ajjnxnxjtx6ci4cp.onion:8333
addnode=icbbaj7up3kzk4cl.onion:8333
addnode=n4n5lzeyiyi2755c.onion:8333
addnode=opw6r3lacfd4eyys.onion:8333
addnode=qdwrjk2ow3yjckne.onion:8333
addnode=wjpkhpyt2gd4jtid.onion:8333
addnode=k7ktw2e5mj4r4na5.onion:8333
addnode=zilzmq3jbzdj63md.onion:8333
addnode=4nnijtmfxeueix7s.onion:8333
addnode=s4gy4h3u5zjmpbtr.onion:8333
addnode=33vdhmzdfimfgov3.onion:8333
addnode=aipupphit3enggpj.onion:8333

