#!/usr/bin/env bash

# install-syncthing is a shell script to install the syncthing file synchronisation utility.

# Copyright (C) 2023 Chris Morrison

# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/.

script_name=$(basename "$0")
script_root=$(dirname -- "$(readlink -f -- "$0";)";)
work_path="${script_root}/${script_name}-build"

function cleanup()
{
    cd "${script_root}"
    if [[ -d "${work_path}" ]]; then
        rm -fr "${work_path}"
    fi
}

if [[ "$(whoami)" != "root" ]]; then
    echo "This script must be run as root." &>2
    exit 1
fi

trap cleanup EXIT
trap "cleanup; exit 1" TERM
trap 'echo -e "\rOperation was cancelled by the user."; cleanup; exit 1' INT

echo "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
echo " INSTALLING SYNCTHING"
echo "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

mkdir -p "${work_path}" || exit 1
cd "${work_path}" || exit 1

echo "Installing dependencies..."

os_name=""
ext=""

case "$(uname -s)" in

    Linux)
        os_name="linux"
        ext="\.tar\.gz"
        if command -v dnf &>/dev/null; then
            dnf -best --assumeyes install git || exit 1
        elif command -v apt-get &>/dev/null; then
            apt-get update || exit 1
            apt-get -y upgrade || exit 1
            apt-get -y install git || exit 1
        else
            echo "Error: unsupported package manager." &>2
            exit 1 
        fi   
        ;;
    FreeBSD)
        ext="\.tar\.gz"
        os_name="freebsd"
        ;;
    NetBSD)
        ext="\.tar\.gz"
        os_name="netbsd"
        ;;
    DragonFly)
        ext="\.tar\.gz"
        os_name="dragonfly"
        ;;
    Darwin)
        ext="\.pkg"
        os_name="darwin"
        ;;
    illumos)
        ext="\.tar\.gz"
        os_name="illumos"
        ;;
    *)
        os_name="unsupported"
        ;;
esac

"${script_root}/install-golang" || exit 1
export PATH="/usr/local/go/bin:${PATH}" # Ugly, temporary hack as PATH will not be up to date yet.

function linux_postflight()
{
    systemctl --user daemon-reload
    systemctl --user enable syncthing || return 1
    systemctl --user start syncthing || return 1
    systemctl --user status --no-pager syncthing || return 1

    sd -F "<address>127.0.0.1:8384</address>" "<address>localhost:8384</address>" "${HOME}/.config/syncthing/config.xml"
    sd -F "<address>[::1]:8384</address>" "<address>localhost:8384</address>" "${HOME}/.config/syncthing/config.xml"
}

echo "Accquiring sources..."
git clone https://github.com/syncthing/syncthing.git || exit 1
cd syncthing || exit 1
go run build.go -no-upgrade || exit 1
if [[ "${os_name}" == "linux" ]]; then
    cp -rf etc/linux-systemd/system /etc/systemd || exit 1
    cp -rf etc/linux-systemd/user /etc/systemd || exit 1
    find man -iname '*.1' -type f -exec cp -f {} /usr/share/man/man1 \;
    find man -iname '*.5' -type f -exec cp -f {} /usr/share/man/man5 \;
    find man -iname '*.7' -type f -exec cp -f {} /usr/share/man/man7 \;
    cp -rf bin /usr || exit 1
    sizes="8 16 20 22 24 32 36 40 42 44 48 64 72 80 96 128 150 192 256 310 512 1024"
    for s in ${sizes}; do
        cp -f assets/logo-${s}.png /usr/share/icons/hicolor/${s}x${s}/apps/syncthing.png &>/dev/null
    done
    cp -f assets/logo-only.svg /usr/share/icons/hicolor/scalable/apps/syncthing.svg
    cp -f etc/linux-desktop/syncthing-ui.desktop /usr/share/applications || exit 1

    echo "fs.inotify.max_user_watches=1073741824" | tee /etc/sysctl.d/90-syncthing.conf
    firewall-cmd --add-service=syncthing --permanent || exit 1
    firewall-cmd --add-service=syncthing-gui --permanent || exit 1
    firewall-cmd --reload || exit 1
    
    export -f linux_postflight
    su - ${SUDO_USER} -c "linux_postflight" || exit 1
fi