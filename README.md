# bitcoin-installer



## Description

This is a collection of shell scripts to make the setting up of workstations, self-host servers and cryptocoin nodes easier.

## Usage
To use, simply clone this repository and run the script for the task you with to complete.

## Authors and acknowledgment
Copyright (c) 2023, Chris Morrison

## License
For licensing information, please see [LICENSE](LICENSE)

